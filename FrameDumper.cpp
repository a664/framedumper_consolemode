/*
* COPYRIGHT (C) 2005 - 2013
*          GE Intelligent Platforms Embedded Systems, Inc.
*          ALL RIGHTS RESERVED.
*
*          THIS SOFTWARE IS FURNISHED UNDER A LICENSE AND MAY BE USED AND
*          COPIED ONLY IN ACCORDANCE WITH THE TERMS OF SUCH LICENSE AND WITH
*          THE INCLUSION OF THE ABOVE COPYRIGHT NOTICE.  THIS SOFTWARE OR ANY
*          OTHER COPIES THEREOF MAY NOT BE PROVIDED OR OTHERWISE MADE
*          AVAILABLE TO ANY OTHER PERSON.  NO TITLE TO AND OWNERSHIP OF THE
*          SOFTWARE IS HEREBY TRANSFERRED.
*
*          THE INFORMATION IN THIS SOFTWARE IS SUBJECT TO CHANGE WITHOUT
*          NOTICE AND SHOULD NOT BE CONSTRUED AS A COMMITMENT BY GE INTELLIGENT PLATFORMS
*          Embedded Systems, Inc.
*
*
*    This test lists the available adapters, then it asks the user for
*    - an adapter to capture from
*    - the name of a trace file where to save packets.
*    Then it opens the adapter and the trace file, it captures packets
*    and dumps them to the trace file in NTAR format. The program ends when
*    the user hits a key.
*
*
* Demonstrated functions:
*  - cpcap_getadapters
*  - cpcap_freeadapters
*  - cpcap_open
*  - cpcap_close
*  - cpcap_getlasterr
*  - cpcap_set_trace_info
*  - cpcap_getnextpacket
*  - cpcap_send_packet
*
*
* Author: Gianluca Varenni, March 2005
*
*/

#include <cpcap.h>
#include <stdio.h>
#include "test_include.h"
#include <vector>
#include <boost/thread.hpp>
//#include <boost/thread/thread.hpp>
//#include <boost/timer.hpp>
//#include <boost/filesystem.hpp>
//#include <boost/property_tree/ptree.hpp>
//#include <boost/property_tree/xml_parser.hpp>

using namespace std;



void TestThread()
{
	while (true)
	{
		//test
		//cout << "1 ";
		//boost::this_thread::sleep(boost::posix_time::millisec(500));
	}
}

void TestThreadSecond(int num)
{
	while (true)
	{
		//cout << num << " ";
		//boost::this_thread::sleep(boost::posix_time::millisec(500));
	}
}

void TestThreadThird(int num, int num2)
{
	while (true)
	{
		//cout << num << " ";
		//boost::this_thread::sleep(boost::posix_time::millisec(500));
	}
}



cpcap_if *list, *list_iterator;
cpcap_if* chosen_if;
cpcap_t* input_dev1;
cpcap_t* output_dev1;
cpcap_t* input_dev2;
cpcap_t* output_dev2;
cpcap_trace_info tinfo;
u_int32 chosen, i;
char errbuf[CPCAP_ERRBUFSIZE];
char filename1[1024];
char filename2[1024];
char* chosen_if_name;
char* chosen_if_name3;


cpcap_pktinfo tx_header1, *prx_header1;
cpcap_pktinfo tx_header2, *prx_header2;
u_int8 *data;
int read_ret_value;

int n_current1 = 0;
int n_write1 = 0;
int c_idx1 = 0;
int w_idx1 = 0;
int n_current2 = 0;
int n_write2 = 0;
int c_idx2 = 0;
int w_idx2 = 0;

int NUM_BUFFERS = 0;
bool running1 = true;
bool running2 = true;
HANDLE hFile;
DWORD result;
DWORD result2;
/// The file location to keep.
OVERLAPPED overlapped_structure;

unsigned long SINGLE_BUFFER = 8;
FILE *fc;


std::vector<u_int8*> buffer1;
std::vector<cpcap_pktinfo*> buffer_header1;

std::vector<u_int8*> buffer2;
std::vector<cpcap_pktinfo*> buffer_header2;

u_int64 start_time1, stop_time1, first_start_time1;
u_int64 start_time2, stop_time2, first_start_time2;
unsigned int cnt1 = 0;
unsigned int cnt2 = 0;

unsigned long totalSize1 = 0;
unsigned long totalSize2 = 0;






void getNext()
{
	while (true)
	{
		if (!running1)
			return;

		cpcap_pktinfo tx_header;
		cpcap_pktinfo* prx_header;
		u_int8 *data;
		int read_ret_value;

		read_ret_value = cpcap_getnextpacket(input_dev1, &prx_header, &data);
		cnt1++;

		if (read_ret_value == CPCAP_READ_OK)
		{
			if (n_write1 + NUM_BUFFERS <= n_current1)
			{
				Sleep(10);
			}

			else
			{
				tx_header = *prx_header;
				tx_header.tx.tx_err_inj_op_info = 0;

				// 10-25-2016 Check Dropped packets between previous and current getnextpacket : Youngsoo
				// Cpcap User's manual p.11
				if (tx_header.rx.rx_dropped_count != 0)
				{
					printf("%5d Packet Dropped - counter %5d.\n", tx_header.rx.rx_dropped_count, cnt1);
				}

				c_idx1 = n_current1 % NUM_BUFFERS;

				memcpy(buffer_header1[c_idx1], prx_header, SINGLE_BUFFER);
				memcpy(buffer1[c_idx1], data, SINGLE_BUFFER);

				//[c_idx1] = prx_header;
				//buffer1[c_idx1] = data;
				n_current1++;
			}

		}
	}
	if (read_ret_value == CPCAP_READ_EOF)
	{
		printf("Error receiving a packet, EOF reached on a device.\n");
		return;
	}
	if (read_ret_value == CPCAP_READ_ERROR)
	{
		printf("Error receiving a packet: %s\n", cpcap_getlasterr(input_dev1));
		return;
	}
}

void getNext2()
{
	while (true)
	{
		if (!running2)
			return;

		cpcap_pktinfo tx_header;
		cpcap_pktinfo* prx_header;
		u_int8 *data;
		int read_ret_value;

		read_ret_value = cpcap_getnextpacket(input_dev2, &prx_header, &data);
		cnt2++;

		if (read_ret_value == CPCAP_READ_OK)
		{
			if (n_write2 + NUM_BUFFERS <= n_current2)
			{
				Sleep(10);
			}

			else
			{
				tx_header = *prx_header;
				tx_header.tx.tx_err_inj_op_info = 0;

				// 10-25-2016 Check Dropped packets between previous and current getnextpacket : Youngsoo
				// Cpcap User's manual p.11
				if (tx_header.rx.rx_dropped_count != 0)
				{
					printf("%5d Packet Dropped - counter %5d.\n", tx_header.rx.rx_dropped_count, cnt2);
				}

				c_idx2 = n_current2 % NUM_BUFFERS;

				memcpy(buffer_header2[c_idx2], prx_header, SINGLE_BUFFER);
				memcpy(buffer2[c_idx2], data, SINGLE_BUFFER);

				//buffer_header2[c_idx2] = prx_header;
				//buffer2[c_idx2] = data;
				n_current2++;
			}

		}
	}
	if (read_ret_value == CPCAP_READ_EOF)
	{
		printf("Error receiving a packet, EOF reached on a device.\n");
		return;
	}

	if (read_ret_value == CPCAP_READ_ERROR)
	{
		printf("Error receiving a packet: %s\n", cpcap_getlasterr(input_dev2));
		return;
	}
}

void sendPacket()
{
	bool result;
	start_time1 = get_milliseconds();
	first_start_time1 = get_milliseconds();
	while (true)
	{
		if (n_current1 > n_write1)
		{
			w_idx1 = n_write1 % NUM_BUFFERS;

			// using fwrite
			//char* p = (char*)&buffer[w_idx];
			//memcpy(&overlapped_structure.OffsetHigh, &p[4], 4);
			//memcpy(&overlapped_structure.Offset, p, 4);
			//result = WriteFile(hFile, buffer[w_idx], 1024, NULL, NULL);
			//result = WriteFile(hFile, buffer[w_idx], 16, &result2, NULL);
			//fwrite(buffer[w_idx], SINGLE_BUFFER, SINGLE_BUFFER, fc);
			//n_write++;

			if (cpcap_send_packet(output_dev1, buffer_header1[w_idx1], buffer1[w_idx1]) == CPCAP_SUCCESS)
			{
				n_write1++;
				tx_header1 = *buffer_header1[w_idx1];
				u_int8 test = *buffer1[w_idx1];
				int a = sizeof(test);
				//totalSize1 += (unsigned long)*prx_header.tx.tx_packet_len;
				totalSize1 += tx_header1.tx.tx_packet_len;
				totalSize1 += 20;
			}

			else
			{
				printf("Error sending a packet to file: %s.\n", cpcap_getlasterr(output_dev1));
				return;
			}


			//stop_time1 = get_milliseconds();

			//if (n_write1 == 100000)
			//{
			//	printf("total size: %d byte \n", totalSize1);
			//	running1 = false;
			//	return;
			//}

			if ((stop_time1 - start_time1) > 1900) // check end user control every 2.0 seconds... 
			{
				//int a = n_current1 - n_write1;
				float bufferRate = 0;
				bufferRate = (float)n_current1 - (float)n_write1;
				bufferRate = bufferRate / (float)NUM_BUFFERS;
				bufferRate = bufferRate * 100;
				long speed = totalSize1 / (stop_time1 - first_start_time1);
				unsigned long size = totalSize1 / 1000;

				//printf("Total packets received: %5d total size: %d\n", cnt, totalSize1);
				//std::cout << "gap: " << n_current1 - n_write1 << std::endl;
				std::cout << "1. BufferRate: " << bufferRate << "%  Record Speed : " << speed << "kbyte/s  totalSize : " << size << " kbyte" << std::endl;
				//printf("BufferRate: %-0.2f  Record Speed: %d kbyte/s totalSize: %d kbyte \n", bufferRate, speed, size);

				running1 = !kbhit();
				start_time1 = get_milliseconds();
				if (!running1)
					return;
			}
		}

	}
}

void sendPacket2()
{
	bool result;
	start_time2 = get_milliseconds();
	first_start_time2 = get_milliseconds();
	while (true)
	{
		if (n_current2 > n_write2)
		{
			w_idx2 = n_write2 % NUM_BUFFERS;

			// using fwrite
			//char* p = (char*)&buffer[w_idx];
			//memcpy(&overlapped_structure.OffsetHigh, &p[4], 4);
			//memcpy(&overlapped_structure.Offset, p, 4);
			//result = WriteFile(hFile, buffer[w_idx], 1024, NULL, NULL);
			//result = WriteFile(hFile, buffer[w_idx], 16, &result2, NULL);
			//fwrite(buffer[w_idx], SINGLE_BUFFER, SINGLE_BUFFER, fc);
			//n_write++;

			if (cpcap_send_packet(output_dev2, buffer_header2[w_idx2], buffer2[w_idx2]) == CPCAP_SUCCESS)
			{
				n_write2++;
				tx_header2 = *buffer_header2[w_idx2];
				u_int8 test = *buffer2[w_idx2];
				int a = sizeof(test);
				//totalSize1 += (unsigned long)*prx_header.tx.tx_packet_len;
				totalSize2 += tx_header2.tx.tx_packet_len;
				totalSize2 += 16;
			}
			else
			{
				printf("Error sending a packet to file: %s.\n", cpcap_getlasterr(output_dev2));
				return;
			}

			stop_time2 = get_milliseconds();
			if ((stop_time2 - start_time2) > 2000) // check end user control every 2.0 seconds... 
			{
				//int a = n_current1 - n_write1;
				float bufferRate = 0;
				bufferRate = (float)n_current2 - (float)n_write2;
				bufferRate = bufferRate / (float)NUM_BUFFERS;
				bufferRate = bufferRate * 100;
				long speed = totalSize2 / (stop_time2 - first_start_time2);
				unsigned long size = totalSize2 / 1000;

				//printf("Total packets received: %5d total size: %d\n", cnt, totalSize1);
				//std::cout << "gap: " << n_current1 - n_write1 << std::endl;
				std::cout << "2 BufferRate: " << bufferRate << "%  Record Speed : " << speed << "kbyte/s  totalSize : " << size << " kbyte" << std::endl;
				//printf("BufferRate: %-0.2f  Record Speed: %d kbyte/s totalSize: %d kbyte \n", bufferRate, speed, size);

				running2 = !kbhit();
				start_time2 = get_milliseconds();
				if (!running2)
					return;
			}
		}

	}
}



int main()
{

	//hFile = CreateFile("test", GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_FLAG_NO_BUFFERING, NULL);
	//if (hFile == INVALID_HANDLE_VALUE)
	//{
	//	int a = 0;
	//}

	//fc = fopen("test", "wb");

	//boost::thread th1 = boost::thread(TestThread);
	//boost::thread th2 = boost::thread(TestThreadSecond, 2);
	//th2.join();
	//boost::thread th3 = boost::thread(TestThreadThird, 3, 0);
	//boost::thread th3 = boost::thread(TestThreadThird);
	//boost::thread th1 = boost::thread(boost::bind(&CSampleIO::TestThread, &io));
	//boost::thread th2 = boost::thread(boost::bind(&CSampleIO::TestThreadSecond, &io, 2));
	//boost::thread th3 = boost::thread(boost::bind(&CSampleIO::TestThreadThird, &io, 3, NULL));


	/* This test lists the available adapters, then it asks the user for
	* - an adapter to capture from
	* - the name of a trace file where to save packets.
	* Then it opens the adapter and the trace file, it captures packets
	* and dumps them to the trace file in NTAR format. The program ends when
	* the user hits a key.
	*/

	cpcap_if *list, *list_iterator;

	/* list the adapters */
	if (cpcap_getadapters(&list, errbuf) != CPCAP_SUCCESS)
	{
		printf("ERROR: Unable to list the adapters, error = %s\n", errbuf);
		return -1;
	}

	i = 0;
	for (list_iterator = list; list_iterator != NULL; list_iterator = list_iterator->next)
	{	/* print the list */
		printf("%u. %s\n\t\t%s\n", i, list_iterator->description, list_iterator->name);
		i++;
	}

	//do
	//{
	//	char buffer[16];
	//	buffer[15] = '\0';
	//	/* loop until the user chooses a valid adapter number */
	//	printf("Choose the first adapter(0-%u): ", i-1);
	//	fgets(buffer, sizeof(buffer) - 1, stdin);
	//	sscanf(buffer,"%u", &chosen);
	//}
	//while(chosen < 0 || chosen > i-1);

	//1
	printf("Insert the trace file name 1: ");
	fgets(filename1, sizeof(filename1) - 1, stdin);
	filename1[sizeof(filename1) - 1] = '\0';

	/* Remove the trailing \n */
	if (filename1[strlen(filename1) - 1] == '\n')
		filename1[strlen(filename1) - 1] = '\0';

	//2
	printf("Insert the trace file name 2: ");
	fgets(filename2, sizeof(filename2) - 1, stdin);
	filename2[sizeof(filename2) - 1] = '\0';

	/* Remove the trailing \n */
	if (filename2[strlen(filename2) - 1] == '\n')
		filename2[strlen(filename2) - 1] = '\0';

	// buffer insert and init
	int num = 0;

	std::cout << "Insert the num of buffers: ";
	std::cin >> num;
	NUM_BUFFERS = num;

	buffer1.resize(NUM_BUFFERS, 0);
	buffer_header1.resize(NUM_BUFFERS, 0);

	buffer2.resize(NUM_BUFFERS, 0);
	buffer_header2.resize(NUM_BUFFERS, 0);

	for (unsigned long i = 0; i < NUM_BUFFERS; ++i)
	{
		buffer1[i] = new u_int8[SINGLE_BUFFER];
		buffer_header1[i] = new cpcap_pktinfo[SINGLE_BUFFER];

		buffer2[i] = new u_int8[SINGLE_BUFFER];
		buffer_header2[i] = new cpcap_pktinfo[SINGLE_BUFFER];
	}

	/* scan the adapters list again to obtain the choosen one */
	/*list_iterator = list;
	for (i=0; i != chosen; i++)
	list_iterator = list_iterator->next;

	chosen_if = list_iterator;
	chosen_if_name = list_iterator->name;*/

	list_iterator = list;
	chosen_if = list_iterator;
	chosen_if_name = list_iterator->name;

	/* open the adapter */
	if (cpcap_open(
		chosen_if_name,
		NULL, /* no second adapter name */
		&input_dev1,
		NULL, /* no second cpcap device */
		65535,	/* snaplen  */
		CPCAP_OPEN_DEVS_READ | CPCAP_OPEN_MODE_PROMISCUOUS,
		1000,		/* timeout for reading packets */
		errbuf)
		!= CPCAP_SUCCESS)
	{
		printf("Error opening the device: %s", errbuf);
		cpcap_freeadapters(list);
		return -1;
	}

	list_iterator = list_iterator->next;
	chosen_if = list_iterator;
	chosen_if_name = list_iterator->name;

	/* open the adapter 2 */
	if (cpcap_open(
		chosen_if_name,
		NULL, /* no second adapter name */
		&input_dev2,
		NULL, /* no second cpcap device */
		65535,	/* snaplen  */
		CPCAP_OPEN_DEVS_READ | CPCAP_OPEN_MODE_PROMISCUOUS,
		1000,		/* timeout for reading packets */
		errbuf)
		!= CPCAP_SUCCESS)
	{
		printf("Error opening the device: %s", errbuf);
		cpcap_freeadapters(list);
		return -1;
	}

	/* open the trace file */
	if (cpcap_open(
		filename1,
		NULL,
		&output_dev1,
		NULL,
		65535,	/* snaplen. We dump the entire packet. Change this to save only a snaplen  */
		CPCAP_OPEN_DEVS_WRITE,
		0,		/* no timeout for writing packets */
		errbuf)
		!= CPCAP_SUCCESS)
	{
		printf("Error opening the file: %s", errbuf);
		cpcap_freeadapters(list);
		cpcap_close(input_dev1);
		return -1;
	}


	/* open the trace file  2 */
	if (cpcap_open(
		filename2,
		NULL,
		&output_dev2,
		NULL,
		65535,	/* snaplen. We dump the entire packet. Change this to save only a snaplen  */
		CPCAP_OPEN_DEVS_WRITE,
		0,		/* no timeout for writing packets */
		errbuf)
		!= CPCAP_SUCCESS)
	{
		printf("Error opening the file: %s", errbuf);
		cpcap_freeadapters(list);
		cpcap_close(input_dev2);
		return -1;
	}

	/* let's zero all the stuff for the traceinfo */
	memset(&tinfo, 0, sizeof(tinfo));

	/* set the link type for the trace */
	tinfo.linktype = chosen_if->linktype;

	/* set the frame check sequence length for the trace */
	tinfo.fcslen = 4;

	/* set the trace info. This is needed to set
	* a proper linktype for the trace file.
	*/
	if (cpcap_set_trace_info(output_dev1, tinfo) != CPCAP_SUCCESS)
	{
		printf("ERROR setting the trace info: %s", cpcap_getlasterr(output_dev1));
		cpcap_close(input_dev1);
		cpcap_close(output_dev1);
		cpcap_freeadapters(list);
		return -1;
	}


	if (cpcap_set_trace_info(output_dev2, tinfo) != CPCAP_SUCCESS)
	{
		printf("ERROR setting the trace info: %s", cpcap_getlasterr(output_dev2));
		cpcap_close(input_dev2);
		cpcap_close(output_dev2);
		cpcap_freeadapters(list);
		return -1;
	}
	printf("Press a key to stop capturing...\n");

	//total time check
	long totalTime = 0;
	clock_t start = clock();

	// thread start
	boost::thread th1 = boost::thread(getNext);
	boost::thread th2 = boost::thread(sendPacket);
	//boost::thread th3 = boost::thread(getNext2);
	//boost::thread th4 = boost::thread(sendPacket2);

	th1.join();
	th2.join();
	//th3.join();
	//th4.join();
	//fclose(fc);

	clock_t end = clock();
	totalTime = end - start;

	std::cout << "total time  : " << totalTime << " msec" << std::endl;
	system("pause");


	/*do
	{
	cpcap_pktinfo tx_header, *prx_header;
	u_int8 *data;
	int read_ret_value;

	read_ret_value = cpcap_getnextpacket(input_dev, &prx_header, &data);

	if (read_ret_value == CPCAP_READ_OK)
	{
	tx_header = *prx_header;
	tx_header.tx.tx_err_inj_op_info = 0;

	if (cpcap_send_packet(output_dev, &tx_header , data) != CPCAP_SUCCESS)
	{
	printf("Error sending a packet to file: %s.\n", cpcap_getlasterr(output_dev));
	break;
	}
	}

	if (read_ret_value == CPCAP_READ_EOF)
	{
	printf("Error receiving a packet, EOF reached on a device.\n");
	break;
	}

	if (read_ret_value == CPCAP_READ_ERROR)
	{
	printf("Error receiving a packet: %s\n", cpcap_getlasterr(input_dev));
	break;
	}
	}
	while (!kbhit());
	*/
	//if (cpcap_close(input_dev) != CPCAP_SUCCESS)
	//	printf("Error closing the input device.\n");

	//if (cpcap_close(output_dev) != CPCAP_SUCCESS)
	//	printf("Error closing the output device.\n");


	cpcap_freeadapters(list);



	return 0;
}
