/*
 *
 * COPYRIGHT (C) 2004 - 2013
 *          GE Intelligent Platforms Embedded Systems, Inc.
 *          ALL RIGHTS RESERVED.
 *
 *          THIS SOFTWARE IS FURNISHED UNDER A LICENSE AND MAY BE USED AND
 *          COPIED ONLY IN ACCORDANCE WITH THE TERMS OF SUCH LICENSE AND WITH
 *          THE INCLUSION OF THE ABOVE COPYRIGHT NOTICE.  THIS SOFTWARE OR ANY
 *          OTHER COPIES THEREOF MAY NOT BE PROVIDED OR OTHERWISE MADE
 *          AVAILABLE TO ANY OTHER PERSON.  NO TITLE TO AND OWNERSHIP OF THE
 *          SOFTWARE IS HEREBY TRANSFERRED.
 *
 *          THE INFORMATION IN THIS SOFTWARE IS SUBJECT TO CHANGE WITHOUT
 *          NOTICE AND SHOULD NOT BE CONSTRUED AS A COMMITMENT BY GE INTELLIGENT PLATFORMS
 *          Embedded Systems, Inc.
 *
 */

#ifndef __TEST_INCLUDE_H_3837273673873478
#define __TEST_INCLUDE_H_3837273673873478

#include <cpcap.h>

#ifdef WIN32
#include <time.h>
#include <conio.h>

static __inline u_int64 get_milliseconds()
{
	clock_t time;
	u_int64 result;

	time = clock();

	result = (u_int64)time * 1000 / CLK_TCK;

	return result;
}

#define U64_FORMATTER "%I64u"
#define U64_CONSTANT(a) a


#elif defined __vxworks
#include "stdio.h"
#include "timers.h"
#include "ioLib.h"

static u_int64 get_milliseconds()
{
	struct timespec current_time;
	u_int64 number;

	if (clock_gettime(CLOCK_REALTIME, &current_time) != OK){
		printf("Hun Hun pas bon!\n");
		return 0;
	}

	number = (current_time.tv_nsec / 1000000) + ((u_int64)current_time.tv_sec) * 1000;

	return number;
}


static int kbhit(void) 
{ 

	int bytesWaiting;
/*
TSS	Set the console for normal terminal operation, except allow immediate
TSS	processing of all inputs.  This operation also causes all waiting
TSS	inputs to be flushed, reducing the chance that a stray input will be
TSS	interpreted as an intentional break request.  
*/
	ioctl(0,FIOSETOPTIONS,OPT_TERMINAL); //^OPT_LINE);

/*
TSS	Calculate the tick at which to continue booting, and wait until the
TSS	timeout period has expired or a key is pressed.
*/
	ioctl(0,FIONREAD,(int)&bytesWaiting);

/*
TSS	Return the console to normal terminal operation.  This operation also
TSS	causes all waiting inputs to be flushed.
*/
	ioctl(0,FIOSETOPTIONS,OPT_TERMINAL);

    return bytesWaiting; 
}


#ifndef Sleep
#define Sleep(a) usleep((a) * 1000)
#endif

#define _ftime ftime
#define _timeb timeb

#define U64_FORMATTER "%llu"
#define U64_CONSTANT(a) a##LL

#else
#include <time.h>
#include <sys/times.h>
#include <termios.h>
#include <unistd.h>

static	inline int getch( ) 
{
	struct termios oldt,
		newt;
	int ch;
	tcgetattr( STDIN_FILENO, &oldt );
	newt = oldt;
	newt.c_lflag &= ~( ICANON | ECHO );
	tcsetattr( STDIN_FILENO, TCSANOW, &newt );
	ch = getchar();
	tcsetattr( STDIN_FILENO, TCSANOW, &oldt );
	return ch;
}

static inline u_int64 get_milliseconds()
{
	struct tms funk_time;
	clock_t time;

	u_int64 result;

	time = times(&funk_time);

	result = ((u_int64)time * 1000) / CLOCKS_PER_SEC;

	return result;
}

static inline int kbhit(void) 
{ 
    fd_set rfds; 
    struct timeval tv; 
    int retval; 
    struct termios term, oterm; 
    int fd = 0; 


    tcgetattr( fd, &oterm ); 
    memcpy( &term, &oterm, sizeof(term) ); 
    cfmakeraw(&term); 
    tcsetattr( fd, TCSANOW, &term ); 


    /* Watch stdin (fd 0) to see when it has input. */ 
    FD_ZERO(&rfds); 
    FD_SET(0, &rfds); 
    /* Wait up to five seconds. */ 
    tv.tv_sec = 0; 
    tv.tv_usec = 0; 


    retval = select(1, &rfds, NULL, NULL, &tv); 
    /* Don't rely on the value of tv now! */ 


    tcsetattr( fd, TCSANOW, &oterm ); 
    return(retval); 
}

#ifndef Sleep
#define Sleep(a) usleep((a) * 1000)
#endif

#define _ftime ftime
#define _timeb timeb

#define U64_FORMATTER "%llu"
#define U64_CONSTANT(a) a##LL

#endif

#endif //__TEST_INCLUDE_H_383727367387347
